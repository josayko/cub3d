# 42cursus-cub3d

- macOS:
	```
	$> git clone https://gitlab.com/josayko/cub3d.git
	$> cd cub3d/
	$> make
	```

- Debian/Ubuntu based distribution:
	- You must install thhe dependencies first:
	``` 
	$> sudo apt update
	$> sudo apt install clang make libx11-dev libxext-dev libbsd-dev
	```
	- Then git clone the repo and do ```make```

- Windows:
	- Enable and install WSL on your system :
		- https://docs.microsoft.com/en-us/windows/wsl/install-win10
	- Install the dependencies (clang, make, libx11, libxext and libbsd)
	- You need to install Xming, an X display server:
		- https://sourceforge.net/projects/xming/
	- enable bash to use Xming by adding ```export DISPLAY=localhost:0.0``` to your .bashrc (or .zshrc, etc...). Then restart your terminal session.
	- git clone the repo and ```make```. Ensure that Xming is running when you launch ```cub3d``` binary !
