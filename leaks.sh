#!bin/sh

EXEC=cub3D
ARGS=map/map.cub
SAVE=--save
valgrind --tool=memcheck --leak-check=full --leak-resolution=high --show-reachable=yes --log-file=valgrind.log ./$EXEC $ARGS #$SAVE
grep -A1 "valgrind" valgrind.log | grep $EXEC
